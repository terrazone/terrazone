package main

import (
	"net"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/terrazone/terrazone/internal/repo/loc"
	"gitlab.com/terrazone/terrazone/internal/repo/mine"
	"gitlab.com/terrazone/terrazone/internal/repo/user"
	"gitlab.com/terrazone/terrazone/internal/server"
)

func main() {
	logConfig := zap.NewDevelopmentConfig()
	logConfig.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	rlog, _ := logConfig.Build()
	log := rlog.Sugar()

	ln, err := net.Listen("tcp", ":5190")
	if err != nil {
		log.Fatalw("cannot listen on 5190 port", "err", err)
	}

	lr, err := loc.NewTzMapRepo()

	if err != nil {
		log.Fatalw("cannot create map repository", "err", err)
	}

	mr, err := mine.NewMineRepo()

	if err != nil {
		log.Fatalw("cannot create mine repository", "err", err)
	}

	ur, err := user.NewDbRepo()
	if err != nil {
		log.Fatalw("cannot create user repository", "err", err)
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Warnw("problems with accepting connection", "err", err)
			continue
		}

		go server.HandleSession(server.HandlerConfig{
			MineRepo: mr,
			UserRepo: ur,
			LocRepo:  lr,
			Conn:     conn,
			Log:      log,
		})
	}
}
