package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"math"
	"os"
	"strings"

	"github.com/urfave/cli/v2"

	"gitlab.com/terrazone/terrazone/pkg/tzmap"
)

func saveMap(m *tzmap.TzMap, fileName string) error {
	upLeft := image.Point{0, 0}
	lowRight := image.Point{360, 360}

	rect := image.Rectangle{upLeft, lowRight}
	img := image.NewRGBA(rect)
	radImg := image.NewRGBA(rect)
	maskImg := image.NewRGBA(rect)

	for x := 0; x < 361; x++ {
		for y := 0; y < 361; y++ {
			clr := color.RGBA{}
			switch m[x][y].Type {
			case 0:
				clr = color.RGBA{255, 0, 0, 255}
			case 1:
				clr = color.RGBA{159, 146, 104, 255}
			case 2:
				clr = color.RGBA{127, 194, 123, 255}
			case 3:
				clr = color.RGBA{228, 222, 110, 255}
			}
			if m[x][y].Bot != 0 || m[x][y].BotCount != 0 {
				clr = color.RGBA{157, 160, 237, 255}
			}
			if m[x][y].Road != 0 {
				clr = color.RGBA{117, 54, 37, 255}
			}

			img.Set(x, y, clr)
			radImg.Set(x, y, color.RGBA{217, 72, 15, 255})
			rad := uint8(math.Pow(float64(m[x][y].Radiation)/127.0, 0.4) * 255.0)
			maskImg.Set(x, y, color.RGBA{rad, rad, rad, rad})
		}
	}

	f, _ := os.Create(fileName)
	draw.DrawMask(img, rect, radImg, image.Point{}, maskImg, image.Point{}, draw.Over)
	png.Encode(f, img)

	return nil
}

func run(tzmFile string, pngFile string) error {
	file, err := os.Open(tzmFile)
	if err != nil {
		fmt.Printf("Cannot open file: %s\n", err)
		return err
	}

	m, err := tzmap.LoadMap(file)
	if err != nil {
		fmt.Printf("Cannot load map from file: %s\n", err)
		return err
	}

	err = saveMap(m, pngFile)
	if err != nil {
		fmt.Printf("Cannot save png image to file: %s\n", err)
		return err
	}

	return nil
}

func getPngFile(tzmFile string) string {
	if strings.HasSuffix(tzmFile, ".tzm") {
		tzmFile = tzmFile[:len(tzmFile)-4]
	}

	return fmt.Sprintf("%s.%s", tzmFile, "png")
}

func main() {
	var pngFile string

	app := &cli.App{
		Name:      "mapper",
		Usage:     "generate image preview of tzm map",
		UsageText: "mapper [-o PNGFILE] [TZMFILE]",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "o",
				Usage:       "path to png `PNGFILE` for save",
				Destination: &pngFile,
			},
		},
		Action: func(c *cli.Context) error {
			tzmFile := "map.tzm"

			if c.NArg() > 0 {
				tzmFile = c.Args().Get(0)
			}

			if pngFile == "" {
				pngFile = getPngFile(tzmFile)
			}

			return run(tzmFile, pngFile)
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		os.Exit(-1)
	}
}
