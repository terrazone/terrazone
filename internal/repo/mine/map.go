package mine

import (
	"context"
)

var (
	mineMap = []uint16{363, 361, 359, 357, 353, 352, 354, 358, 360, 362, 364, 299, 297, 295, 293, 291, 289, 290, 292, 294, 296, 298, 300, 225, 226, 171, 169, 167, 165, 163, 161, 162, 164, 166, 168, 107, 103, 97, 96, 98, 104, 43, 41, 39, 37, 33, 32, 34, 38, 40, 42, 44, 5, 3, 1, 0, 2, 4, 6, 12, 75, 73, 71, 69, 65, 64, 66, 70, 72, 76, 139, 135, 129, 128, 130, 136, 140, 203, 201, 199, 197, 195, 193, 194, 196, 198, 200, 202, 204, 257, 258, 331, 329, 327, 325, 323, 321, 322, 324, 326, 328, 330, 332, 395, 393, 391, 389, 387, 385, 384, 390, 392, 394}
)

type mineRepo struct {
	m [][]AditType
}

func NewMineRepo() (*mineRepo, error) {
	var mr mineRepo

	mr.m = make([][]AditType, 30)
	for i := 0; i < 30; i++ {
		mr.m[i] = make([]AditType, 30)
		for j := 0; j < 30; j++ {
			mr.m[i][j] = Wall
		}
	}

	for _, room := range mineMap {
		x, y := RoomToXY(room)
		mr.m[x+15][y+15] = CommonAdit
	}

	return &mr, nil
}

func (mr *mineRepo) GetByXY(ctx context.Context, x int16, y int16) (Mine, error) {
	return Mine{
		X:   x,
		Y:   y,
		Map: mr.m,
	}, nil
}
