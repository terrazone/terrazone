package mine

import (
	"context"
)

type AditType uint8

const (
	CommonAdit AditType = iota
	Wall
	Subway
)

type Mine struct {
	X   int16
	Y   int16
	Map [][]AditType
}

type Repo interface {
	GetByXY(ctx context.Context, x int16, y int16) (Mine, error)
}

func RoomToXY(room uint16) (int16, int16) {
	x := int16(room) & 31
	y := (int16(room) >> 5) & 31

	if x%2 != 0 {
		x = (-x - 1) / 2
	} else {
		x /= 2
	}
	if y%2 != 0 {
		y = (-y - 1) / 2
	} else {
		y /= 2
	}

	return x, y
}

func XYToRoom(x int16, y int16) uint16 {
	if x < 0 {
		x = (-x)*2 - 1
	} else {
		x *= 2
	}

	if y < 0 {
		y = (-y)*2 - 1
	} else {
		y *= 2
	}

	return uint16(x) + (uint16(y) << 5)
}
