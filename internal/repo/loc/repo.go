package loc

import (
	"context"
)

type LocationType uint8

const (
	Water LocationType = iota
	Stone
	Grass
	Desert
)

type BuildingType uint8

const (
	_ BuildingType = iota

	Arena
	Arsenal
	Shop
	Bank
	Hospital
	Labyrinth
	CityHall
	ClanOffice
	Post
	Lab
	Portal
	Factory
	Mine
	Tribunal
	Pub
	Police
	AbadonedMine
	Univercity
)

type Building struct {
	X    uint8
	Y    uint8
	Z    uint8
	Type BuildingType
	Name string
}

type Location struct {
	X         int16
	Y         int16
	Type      LocationType
	Dome      bool
	Road      uint8
	Radiation uint8
	Buildings []Building
	Map       string
}

type Repo interface {
	GetByXY(ctx context.Context, x int16, y int16) (Location, error)
}

func (lt LocationType) Code() string {
	switch lt {
	case Grass:
		return "A"
	case Desert:
		return "C"
	case Stone:
		return "B"
	default:
		return "E"
	}
}
