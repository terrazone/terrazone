package user

import (
	"context"
	"fmt"
)

var (
	ErrNotFound = fmt.Errorf("User not found")
)

type Parameters struct {
	Str   uint16
	Dex   uint16
	Int   uint16
	Pow   uint16
	Acc   uint16
	Intel uint16
}

type Description struct {
	Note  string
	City  string
	About string
	Name  string
	Hint  uint16
	Color uint8
}

type Identifiers struct {
	First  uint64
	Second uint64
	Shift  uint8
}

type User struct {
	Login        string
	Password     string
	Hp           uint16
	MaxHp        uint16
	Psy          uint16
	MaxPsy       uint16
	X            int16
	Y            int16
	Z            uint16
	BuildingType uint8
	Room         uint16
	Exp          uint64
	Level        uint8
	Img          string
	Man          uint8
	LocTime      uint32
	God          uint8
	Training1    uint8
	Training2    uint8
	Description
	Identifiers
	Parameters
}

type Repo interface {
	GetByLogin(ctx context.Context, login string) (User, error)
	Update(ctx context.Context, user User) error
}
