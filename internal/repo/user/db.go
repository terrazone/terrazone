package user

import (
	"context"
)

type dbRepo struct {
	user User
}

func NewDbRepo() (*dbRepo, error) {
	return &dbRepo{
		user: User{
			Password: "12345",
			Hp:       80,
			MaxHp:    100,
			Exp:      40,
			Level:    1,
			Man:      1,
			God:      1,
			X:        56,
			Y:        34,
			Parameters: Parameters{
				Str:   3,
				Dex:   3,
				Int:   3,
				Pow:   3,
				Acc:   0,
				Intel: 0,
			},
			Identifiers: Identifiers{
				Second: 100,
			},
		},
	}, nil
}

func (d *dbRepo) GetByLogin(ctx context.Context, login string) (User, error) {
	d.user.Login = login

	return d.user, nil
}

func (d *dbRepo) Update(ctx context.Context, user User) error {
	d.user = user

	return nil
}
