package start

import (
	"encoding/xml"
)

type ErrorCode uint32

const (
	_ ErrorCode = iota
	ErrUserNotFound
	ErrWrongPassword
	ErrAlreadyLogined
	ErrUserBlocked
	ErrOldVersion
	ErrKeyNeeded
	ErrWrongKey
	ErrOpenedWindow
	ErrBroken
)

type Error struct {
	XMLName xml.Name  `xml:"ERROR"`
	Code    ErrorCode `xml:"code,attr"`
}

type Ok struct {
	XMLName xml.Name `xml:"OK"`
	Login   string   `xml:"l,attr"`
	Session string   `xml:"ses,attr"`
}
