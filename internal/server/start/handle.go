package start

import (
	"context"
	"encoding/xml"
	"fmt"

	"gitlab.com/terrazone/terrazone/internal/repo/user"
	"gitlab.com/terrazone/terrazone/internal/server/common"
	"gitlab.com/terrazone/terrazone/pkg/password"
)

type handler struct {
	common.HandlerConfig
	state *common.HandlerState
}

func NewHandler(hc common.HandlerConfig, state *common.HandlerState) handler {
	return handler{
		HandlerConfig: hc,
		state:         state,
	}
}

func (h handler) handleLogin(login Login) error {
	h.Log.Debugw("got login", "in", login)

	u, err := h.UserRepo.GetByLogin(context.Background(), login.Login)
	if err == user.ErrNotFound {
		err = h.Send(Error{
			Code: ErrUserNotFound,
		})

		return err
	} else if err != nil {
		err = h.Send(Error{
			Code: ErrBroken,
		})

		return err
	}

	// TODO: allow undefined only on dev
	if password.Encrypt(u.Password, h.state.Salt) != login.Password && login.Password != "undefined" {
		err = h.Send(Error{
			Code: ErrWrongPassword,
		})

		return err
	}

	h.state.User = u

	// TODO: generate session key
	err = h.Send(Ok{
		Login:   h.state.User.Login,
		Session: "eeeee",
	})

	if err != nil {
		return err
	}

	return nil
}

func (h handler) handleChat(chat Chat) error {
	h.Log.Debugw("got chat", "in", chat)

	return nil
}

func (h handler) Handle(data []byte) error {
	var ip InputPackage

	err := xml.Unmarshal(data, &ip)

	// TODO: wrap error
	if err != nil {
		return err
	}

	switch v := ip.Content.(type) {
	case *Login:
		err = h.handleLogin(*v)
	case *Chat:
		err = h.handleChat(*v)
	default:
		// TODO: add type for error
		return fmt.Errorf("unknown content %v", v)
	}

	// TODO: wrap error
	if err != nil {
		return err
	}

	return nil
}
