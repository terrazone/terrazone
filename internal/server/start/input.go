package start

import (
	"encoding/xml"
	"fmt"
	"io"
)

type Start struct{}

type Login struct {
	XMLName  xml.Name `xml:"LOGIN"`
	Login    string   `xml:"l,attr"`
	Password string   `xml:"p,attr"`
	Version  string   `xml:"v,attr"`
	Version2 string   `xml:"v2,attr"`
	Lang     string   `xml:"lang,attr"`
}

type Chat struct {
	XMLName xml.Name `xml:"CHAT"`
	Server  string   `xml:"server,attr"`
}

type InputPackage struct {
	Content interface{}
}

func (ip *InputPackage) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	switch start.Name.Local {
	case "LOGIN":
		ip.Content = &Login{}
	case "CHAT":
		ip.Content = &Chat{}
	default:
		return fmt.Errorf("cannot parse unknown tag %s while parsing", start.Name.Local)
	}

	err := d.DecodeElement(ip.Content, &start)
	if err == io.EOF {
		return err
	} else if err != nil {
		return fmt.Errorf("problem with parsing: %w", err)
	}

	return nil
}
