package movement

import (
	"context"
	"encoding/xml"

	"gitlab.com/terrazone/terrazone/internal/server/common"
)

type handler struct {
	common.HandlerConfig
	state *common.HandlerState
}

func NewHandler(hc common.HandlerConfig, state *common.HandlerState) handler {
	return handler{
		HandlerConfig: hc,
		state:         state,
	}
}

func getDirection(n uint8) (int16, int16) {
	if n > 9 {
		return 0, 0
	}

	m := int16(n)
	y := (m-1)/3 - 1
	x := (m-1)%3 - 1

	return x, y
}

func (h handler) handleGoLoc(goLoc GoLoc) error {
	var bot, road uint8

	h.Log.Debugw("got goloc", "in", goLoc)

	out := GoLoc{}

	if goLoc.Destination != 0 {
		x, y := getDirection(goLoc.Destination)
		h.state.User.X += x
		h.state.User.Y += y
		out.Destination = goLoc.Destination
	}

	h.UserRepo.Update(context.Background(), h.state.User)

	for _, d := range goLoc.Compass {
		x, y := getDirection(uint8(d - '0'))

		l, err := h.LocRepo.GetByXY(context.Background(), h.state.User.X+x, h.state.User.Y+y)

		if err != nil {
			h.Log.Warnw("cannot get info about location", "x", x, "y", y, "err", err)
			continue
		}

		bot = 1
		if l.Dome {
			bot = 0
		}

		road = 0
		if l.Road > 0 {
			road = 1
		}

		x, y = l.X, l.Y
		if x < 0 {
			x += 360
		}
		if y < 0 {
			y += 360
		}

		bs := make([]Building, len(l.Buildings))

		for i, b := range l.Buildings {
			bs[i] = Building{
				X:    b.X,
				Y:    b.Y,
				Z:    b.Z,
				Type: uint8(b.Type),
				Name: b.Name,
			}
		}

		out.Locations = append(out.Locations, Location{
			X:           x,
			Y:           y,
			Type:        l.Type.Code(),
			Bot:         bot,
			Road:        road,
			RoadQuality: l.Road,
			Map:         l.Map,
			Building:    bs,
			Z:           100,
		})

		if err != nil {
			h.Log.Warnw("cannot send info about location", "x", x, "y", y, "err", err)
		}
	}

	return h.Send(out)
}

func (h handler) Handle(data []byte) error {
	var ip InputPackage

	err := xml.Unmarshal(data, &ip)

	// TODO: wrap error
	if err != nil {
		return h.state.CommonHandler.Handle(data)
	}

	switch v := ip.Content.(type) {
	case *GoLoc:
		err = h.handleGoLoc(*v)
	default:
		// TODO: add type for error
		return h.state.CommonHandler.Handle(data)
	}

	// TODO: wrap error
	if err != nil {
		return err
	}

	return nil
}
