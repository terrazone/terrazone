package movement

import (
	"encoding/xml"
)

type Mine struct {
	XMLName xml.Name `xml:"MN"`
	P1      int32    `xml:"p1,attr"`
	P2      int32    `xml:"p2,attr"`
}

type Building struct {
	X    uint8  `xml:"X,attr"`
	Y    uint8  `xml:"Y,attr"`
	Z    uint8  `xml:"Z,attr"`
	Type uint8  `xml:"name,attr"`
	Name string `xml:"txt,attr"`
}

type Location struct {
	XMLName     xml.Name   `xml:"L"`
	Time        int64      `xml:"tm,attr"`
	Type        string     `xml:"t,attr"`
	X           int16      `xml:"X,attr"`
	Y           int16      `xml:"Y,attr"`
	Z           uint16     `xml:"z,attr"`
	Bot         uint8      `xml:"b,attr"`
	Map         string     `xml:"m,attr,omitempty"`
	Road        uint8      `xml:"r,attr,omitempty"`
	RoadQuality uint8      `xml:"p,attr"`
	Radiation   int32      `xml:"o,attr"`
	N           int32      `xml:"n,attr"`
	Building    []Building `xml:"B"`
}
