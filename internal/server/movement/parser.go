package movement

import (
	"encoding/xml"
	"fmt"
	"io"
)

type GoLoc struct {
	XMLName     xml.Name   `xml:"GOLOC"`
	Compass     string     `xml:"d,attr,omitempty"`
	Session     string     `xml:"s,attr,omitempty"`
	Destination uint8      `xml:"n,attr,omitempty"`
	GoTime      int64      `xml:"t1,attr,omitempty"`
	PrevTime    int64      `xml:"t2,attr,omitempty"`
	Slow        uint8      `xml:"slow,attr,omitempty"`
	Locations   []Location `xml:"L"`
}

type InputPackage struct {
	Content interface{}
}

func (ip *InputPackage) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	switch start.Name.Local {
	case "GOLOC":
		ip.Content = &GoLoc{}
	default:
		return fmt.Errorf("cannot parse unknown tag %s while parsing", start.Name.Local)
	}

	err := d.DecodeElement(ip.Content, &start)
	if err == io.EOF {
		return err
	} else if err != nil {
		return fmt.Errorf("problem with parsing: %w", err)
	}

	return nil
}
