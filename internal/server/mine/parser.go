package mine

import (
	"fmt"
	"io"

	"encoding/xml"
)

type Mine struct {
	XMLName xml.Name `xml:"HD"`
	Go      uint8    `xml:"go,attr,omitempty"`
	Open    int32    `xml:"open,attr,omitempty"`
	Map     string   `xml:"n,attr,omitempty"`
	Room    uint16   `xml:"r,attr"`
	Timeout uint32   `xml:"t,attr"`
	Door    uint8    `xml:"d,attr,omitempty"`
	Code    uint8    `xml:"code,attr,omitempty"`
}

type InputPackage struct {
	Content interface{}
}

func (ip *InputPackage) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	switch start.Name.Local {
	case "HD":
		ip.Content = &Mine{}
	default:
		return fmt.Errorf("cannot parse unknown tag %s while parsing", start.Name.Local)
	}

	err := d.DecodeElement(ip.Content, &start)
	if err == io.EOF {
		return err
	} else if err != nil {
		return fmt.Errorf("problem with parsing: %w", err)
	}

	return nil
}
