package mine

import (
	"context"
	"encoding/xml"

	"gitlab.com/terrazone/terrazone/internal/repo/mine"
	"gitlab.com/terrazone/terrazone/internal/server/common"
)

type handler struct {
	common.HandlerConfig
	state *common.HandlerState
}

func NewHandler(hc common.HandlerConfig, state *common.HandlerState) handler {
	return handler{
		HandlerConfig: hc,
		state:         state,
	}
}

func parseDirection(d uint8) (int16, int16) {
	switch d {
	case 1:
		return -1, 0
	case 2:
		return 0, -1
	case 3:
		return 1, 0
	case 4:
		return 0, 1
	}

	return 0, 0
}

func prepareMineMap(mn mine.Mine, x int16, y int16) (string, error) {
	buf := make([]byte, 13)

	for i := int16(0); i < 5; i++ {
		for j := int16(0); j < 5; j++ {
			n := (i*5 + j) / 2
			a := uint8(mn.Map[x+j+13][y+i+13])

			if (i*5+j)%2 == 0 {
				buf[n] = a
			} else {
				buf[n] = a<<2 | buf[n]
			}
		}
	}

	for i, b := range buf {
		if b < 10 {
			buf[i] = '0' + b
			continue
		}

		buf[i] = 'A' + b - 10
	}

	return string(buf), nil
}

func (h handler) handleMine(m Mine) error {
	h.Log.Debugw("got HD", "in", m)

	i, j := parseDirection(m.Go)
	x, y := mine.RoomToXY(h.state.User.Room)

	mn, err := h.MineRepo.GetByXY(context.Background(), h.state.User.X, h.state.User.Y)
	if err != nil {
		return nil
	}

	mp, err := prepareMineMap(mn, x+i, y+j)
	if err != nil {
		return err
	}

	h.state.User.Room = mine.XYToRoom(x+i, y+j)
	h.UserRepo.Update(context.Background(), h.state.User)

	return h.Send(Mine{
		Timeout: 0,
		Room:    h.state.User.Room,
		Map:     mp,
	})
}

// Handle GOBLD specially
func (h handler) Handle(data []byte) error {
	var ip InputPackage

	err := xml.Unmarshal(data, &ip)

	// TODO: wrap error
	if err != nil {
		return h.state.CommonHandler.Handle(data)
	}

	switch v := ip.Content.(type) {
	case *Mine:
		err = h.handleMine(*v)
	default:
		// TODO: add type for error
		return h.state.CommonHandler.Handle(data)
	}

	// TODO: wrap error
	if err != nil {
		return err
	}

	return nil
}
