package server

import (
	"bufio"
	"encoding/xml"
	"io"
	"net"

	"go.uber.org/zap"

	"gitlab.com/terrazone/terrazone/internal/repo/loc"
	"gitlab.com/terrazone/terrazone/internal/repo/user"

	"gitlab.com/terrazone/terrazone/internal/server/arsenal"
	"gitlab.com/terrazone/terrazone/internal/server/common"
	"gitlab.com/terrazone/terrazone/internal/server/mine"
	"gitlab.com/terrazone/terrazone/internal/server/movement"
	"gitlab.com/terrazone/terrazone/internal/server/start"
	"gitlab.com/terrazone/terrazone/pkg/password"

	mr "gitlab.com/terrazone/terrazone/internal/repo/mine"
)

type Handler struct {
	HandlerConfig
	state    *common.HandlerState
	enc      *xml.Encoder
	reader   *bufio.Reader
	handlers map[common.HandlerType]common.Handler
}

type HandlerConfig struct {
	Conn     net.Conn
	LocRepo  loc.Repo
	MineRepo mr.Repo
	UserRepo user.Repo
	Log      *zap.SugaredLogger
}

func NewHandler(conf HandlerConfig) (*Handler, error) {
	state := &common.HandlerState{
		Salt: password.GenSalt(),
	}

	enc := xml.NewEncoder(conf.Conn)

	chc := common.HandlerConfig{
		UserRepo: conf.UserRepo,
		MineRepo: conf.MineRepo,
		LocRepo:  conf.LocRepo,
		Log:      conf.Log,
		Conn:     conf.Conn,
		Enc:      enc,
	}

	state.CommonHandler = common.NewHandler(chc, state)

	handlers := map[common.HandlerType]common.Handler{
		common.Start:    start.NewHandler(chc, state),
		common.Movement: movement.NewHandler(chc, state),
		common.Arsenal:  arsenal.NewHandler(chc, state),
		common.Mine:     mine.NewHandler(chc, state),
	}

	return &Handler{
		HandlerConfig: conf,
		reader:        bufio.NewReader(conf.Conn),
		enc:           enc,
		state:         state,
		handlers:      handlers,
	}, nil
}

func (h *Handler) Send(v interface{}) error {
	err := h.enc.Encode(v)
	if err != nil {
		return err
	}

	_, err = h.Conn.Write([]byte{0})

	return err
}

func (h *Handler) SessionLoop(dataCh chan []byte, stopCh chan struct{}) {
	for {
		select {
		case <-stopCh:
			h.Log.Debugw("Session was closed")
			return
			/*		case <-h.state.battleEvent:
					h.Log.Debugw("Updated battle status")

					tp := h.state.GetType()
					if tp == common.Battle {
						(battle.Handler(h.handlers[common.Battle])).Update()
					}
			*/
		case data := <-dataCh:
			tp := h.state.GetType()

			err := h.handlers[tp].Handle(data)
			if err != nil {
				h.Log.Warnw("problem with handle message", "type", tp, "err", err)
			}
		}
	}
}

func HandleSession(conf HandlerConfig) {
	h, err := NewHandler(conf)
	if err != nil {
		h.Log.Warnw("cannot initialize handler", "err", err)
		return
	}

	err = h.Send(Key{
		Salt: h.state.Salt,
	})

	if err != nil {
		h.Log.Warnw("cannot send salt", "err", err)
		return
	}

	dataCh := make(chan []byte)
	stopCh := make(chan struct{})

	go h.SessionLoop(dataCh, stopCh)

	for {
		data, err := h.reader.ReadBytes(byte(0))
		if err == io.EOF {
			close(stopCh)
			close(dataCh)
			return
		} else if err != nil {
			close(stopCh)
			close(dataCh)
			h.Log.Warnw("problem with reading from socket", "err", err)
			return
		}

		dataCh <- data
	}
}
