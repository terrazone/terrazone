package arsenal

import (
	"encoding/xml"
	"fmt"
	"io"

	"gitlab.com/terrazone/terrazone/internal/server/common"
)

type Arsenal struct {
	XMLName xml.Name     `xml:"AR"`
	Level   uint8        `xml:"level,attr,omitempty"`
	Boxes   []common.Box `xml:"O"`
	Add     uint64       `xml:"a,attr,omitempty"`
	Delete  uint64       `xml:"d,attr,omitempty"`
	Count   uint16       `xml:"c,attr,omitempty"`
	Section uint8        `xml:"s,attr,omitempty"`
}

type InputPackage struct {
	Content interface{}
}

func (ip *InputPackage) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	switch start.Name.Local {
	case "AR":
		ip.Content = &Arsenal{}
	default:
		return fmt.Errorf("cannot parse unknown tag %s while parsing", start.Name.Local)
	}

	err := d.DecodeElement(ip.Content, &start)
	if err == io.EOF {
		return err
	} else if err != nil {
		return fmt.Errorf("problem with parsing: %w", err)
	}

	return nil
}
