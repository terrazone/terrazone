package arsenal

import (
	"encoding/xml"

	"gitlab.com/terrazone/terrazone/internal/server/common"
)

type handler struct {
	common.HandlerConfig
	state *common.HandlerState
}

func NewHandler(hc common.HandlerConfig, state *common.HandlerState) handler {
	return handler{
		HandlerConfig: hc,
		state:         state,
	}
}

func (h handler) handleArsenal(arsenal Arsenal) error {
	h.Log.Debugw("got Arsenal", "in", arsenal)

	return h.Send(Arsenal{
		Level: 1,
		Boxes: []common.Box{
			common.Box{
				Id:          76293572255,
				Name:        "ba1-c2",
				Description: "Jeans vest",
				Weight:      50,
				Quality:     20,
				MaxQuality:  20,
				Slot:        "CDE",
			},
			common.Box{
				Id:          76293572256,
				Name:        "ba1-h1",
				Description: "Carrot Beret",
				Weight:      10,
				Quality:     45,
				MaxQuality:  45,
				Slot:        "F",
			},
			common.Box{
				Id:          76293572257,
				Name:        "ba1-b1",
				Description: "Boots",
				Weight:      120,
				Quality:     12,
				MaxQuality:  20,
				Slot:        "B",
			},
			common.Box{
				Id:          76293572258,
				Name:        "ba1-t1",
				Description: "Jeans",
				Weight:      50,
				Quality:     10,
				MaxQuality:  10,
				Slot:        "A",
			},
			common.Box{
				Id:          76293572259,
				Name:        "b1-k2",
				Description: "Sharp knife",
				Weight:      50,
				Slot:        "G,H",
				Shot:        "1-2",
				Damage:      "S2-2",
			},
		},
	})
}

func (h handler) Handle(data []byte) error {
	var ip InputPackage

	err := xml.Unmarshal(data, &ip)

	// TODO: wrap error
	if err != nil {
		return h.state.CommonHandler.Handle(data)
	}

	switch v := ip.Content.(type) {
	case *Arsenal:
		err = h.handleArsenal(*v)
	default:
		// TODO: add type for error
		return h.state.CommonHandler.Handle(data)
	}

	// TODO: wrap error
	if err != nil {
		return err
	}

	return nil
}
