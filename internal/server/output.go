package server

import (
	"encoding/xml"
)

type Key struct {
	XMLName xml.Name `xml:"KEY"`
	Salt    string   `xml:"s,attr"`
}
