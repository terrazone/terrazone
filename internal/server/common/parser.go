package common

import (
	"encoding/xml"
	"fmt"
	"io"
)

type GetMe struct {
	XMLName xml.Name `xml:"GETME"`
}

type Nop struct {
	XMLName xml.Name `xml:"N"`
	Id1     uint64   `xml:"id1,attr"`
	Id2     uint64   `xml:"id2,attr"`
	I1      uint8    `xml:"i1,attr"`
}

type GoBld struct {
	XMLName xml.Name `xml:"GOBLD"`
	Number  uint16   `xml:"n,attr"`
}

type KickAss struct {
	XMLName  xml.Name `xml:"KICKASS"`
	Login    string   `xml:"login,attr"`
	Training uint8    `xml:"training,attr"`
}

type Change struct {
	XMLName xml.Name `xml:"CHANGE"`
	Param   string   `xml:"param,attr"`
	Value   string   `xml:"txt,attr"`
}

type Status struct {
	XMLName xml.Name `xml:"STATUS"`
	Clan    uint8    `xml:"clan,attr,omitempty"`
	List    string   `xml:"list,attr,omitempty"`
	Police  string   `xml:"police,attr,omitempty"`
	Stat    string   `xml:"s,attr"`
}

type GetInfo struct {
	XMLName xml.Name `xml:"GETINFO"`
	Login   string   `xml:"login,attr"`
	UserParam
}

type InputPackage struct {
	Content interface{}
}

func (ip *InputPackage) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	switch start.Name.Local {
	case "CHANGE":
		ip.Content = &Change{}
	case "KICKASS":
		ip.Content = &KickAss{}
	case "GETME":
		ip.Content = &GetMe{}
	case "N":
		ip.Content = &Nop{}
	case "GOBLD":
		ip.Content = &GoBld{}
	case "STATUS":
		ip.Content = &Status{}
	case "GETINFO":
		ip.Content = &GetInfo{}
	default:
		return fmt.Errorf("cannot parse unknown tag %s while parsing", start.Name.Local)
	}

	err := d.DecodeElement(ip.Content, &start)
	if err == io.EOF {
		return err
	} else if err != nil {
		return fmt.Errorf("problem with parsing: %w", err)
	}

	return nil
}
