package common

import (
	"encoding/xml"
)

type Description struct {
	About string `xml:"about,attr,omitempty"`
	City  string `xml:"city,attr,omitempty"`
	Color uint8  `xml:"clr,attr,omitempty"`
	Hint  uint16 `xml:"hint,attr,omitempty"`
	Name  string `xml:"name,attr,omitempty"`
	Note  string `xml:"note,attr,omitempty"`
}

type Identifiers struct {
	First  uint64 `xml:"id1,attr"`
	Second uint64 `xml:"id2,attr"`
	Shift  uint8  `xml:"i1,attr"`
}

type Parameters struct {
	Str   uint16 `xml:"str,attr,omitempty"`
	Dex   uint16 `xml:"dex,attr,omitempty"`
	Int   uint16 `xml:"int,attr,omitempty"`
	Pow   uint16 `xml:"pow,attr,omitempty"`
	Acc   uint16 `xml:"acc,attr,omitempty"`
	Intel uint16 `xml:"intel,attr,omitempty"`
}

// TODO: move comments to repo
type Effects struct {
	Poisoning      uint16 `xml:"poisoning,attr"`     // Отравление
	Paralyzed      uint16 `xml:"paralyzed,attr"`     // Парализация
	Dazzling       uint16 `xml:"dazzling,attr"`      // Ослепление
	Panic          uint16 `xml:"panic,attr"`         // Паника
	Hallucination  uint16 `xml:"hallucination,attr"` // Галлюцинации
	Contusion      uint16 `xml:"contusion,attr"`     // Контузия
	Zombie         uint16 `xml:"zombie,attr"`        // Зомбирование
	Virus          uint16 `xml:"virus,attr"`         // Биологическое заражение
	Nark           uint16 `xml:"nark,attr"`          // Паника
	ODRatio        uint16 `xml:"ODratio,attr"`       // Серьезная травма ног, замедлено перемещение
	Ill            uint16 `xml:"ill,attr"`           // Заражение вирусом X
	DecreaseParams uint16 `xml:"dx_p,attr"`          // Отрицательное воздействие на параметры персонажа
	DecreaseSkills uint16 `xml:"dx_s,attr"`          // Отрицательное воздействие на навыки персонажа
	DecreaseOD     uint16 `xml:"dx_OD,attr"`         // Отрицательное влияние на ОД
	Shock          uint16 `xml:"dp_eff,attr"`        // Реанимационный шок
}

type BasicParam struct {
	Level        uint8  `xml:"level,attr"`
	Hp           uint16 `xml:"HP,attr"`
	MaxHp        uint16 `xml:"maxHP,attr"`
	Psy          uint16 `xml:"psy,attr"`
	MaxPsy       uint16 `xml:"maxPsy,attr"`
	Stamina      uint16 `xml:"stamina,attr"` // TODO: check it
	Profession   uint8  `xml:"pro,attr"`
	ProfPower    uint8  `xml:"propwr,attr"`
	NegEffParams string `xml:"ne,attr,omitempty"`
	NegEffSkills string `xml:"ne2,attr,omitempty"`
	Man          uint8  `xml:"man,attr"`
	X            int16  `xml:"X,attr"`
	Y            int16  `xml:"Y,attr"`
	Effects
	Parameters
}

type UserParam struct {
	XMLName          xml.Name `xml:"USERPARAM"`
	Login            string   `xml:"login,attr"`
	RegDay           string   `xml:"regday,attr,omitempty"`
	BuildingTypeName string   `xml:"hztxt,attr,omitempty"`
	BasicParam
}

type NoUser struct {
	XMLName xml.Name `xml:"NOUSER"`
	Login   string   `xml:"login,attr"`
}

type MyParam struct {
	XMLName      xml.Name `xml:"MYPARAM"`
	Z            uint16   `xml:"Z,attr"`
	BuildingType uint8    `xml:"hz,attr,omitempty"`
	Room         uint16   `xml:"ROOM,attr"`
	Time         int64    `xml:"time,attr"`
	Exp          uint64   `xml:"exp,attr"`
	Login        string   `xml:"login,attr"`
	Img          string   `xml:"img,attr"`
	LocTime      uint32   `xml:"loc_time,attr"`
	God          uint8    `xml:"god,attr,omitempty"`
	Training1    uint8    `xml:"t1,attr,omitempty"`
	Training2    uint8    `xml:"t2,attr,omitempty"`
	BasicParam
	Description
	Identifiers
}

type StopChat struct {
	XMLName xml.Name `xml:"STOPCHAT"`
}

type OkGo struct {
	XMLName xml.Name `xml:"OKGO"`
}

type Box struct {
	Id          uint64  `xml:"id,attr"`
	Name        string  `xml:"name,attr"`
	Description string  `xml:"txt,attr"`
	Weight      float32 `xml:"massa,attr"`
	Section     uint8   `xml:"section,attr"`
	Quality     uint16  `xml:"quality,attr"`
	MaxQuality  uint16  `xml:"maxquality,attr"`
	Slot        string  `xml:"st,attr"`
	Shot        string  `xml:"shot,attr"`
	Damage      string  `xml:"damage,attr"`
}
