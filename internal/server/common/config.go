package common

import (
	"encoding/xml"
	"go.uber.org/zap"
	"net"

	"gitlab.com/terrazone/terrazone/internal/repo/loc"
	"gitlab.com/terrazone/terrazone/internal/repo/mine"
	"gitlab.com/terrazone/terrazone/internal/repo/user"
)

type HandlerType uint16

const (
	Start HandlerType = iota
	Common
	Battle
	Movement
	Arsenal
	Mine
)

type HandlerConfig struct {
	LocRepo  loc.Repo
	MineRepo mine.Repo
	UserRepo user.Repo
	Log      *zap.SugaredLogger
	Conn     net.Conn
	Enc      *xml.Encoder
}

type HandlerState struct {
	User          user.User
	Salt          string
	CommonHandler Handler
}

type Handler interface {
	Handle(data []byte) error
}

func (h *HandlerConfig) Send(v interface{}) error {
	err := h.Enc.Encode(v)
	if err != nil {
		return err
	}

	_, err = h.Conn.Write([]byte{0})

	return err
}

func (h HandlerState) GetType() HandlerType {
	if h.User.Login == "" {
		return Start
	}

	switch loc.BuildingType(h.User.BuildingType) {
	case loc.AbadonedMine:
		return Mine
	case loc.Arsenal:
		return Arsenal
	}

	return Movement
}
