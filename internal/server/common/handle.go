package common

import (
	"context"
	"encoding/xml"
	"fmt"
	"strconv"
	"time"
	// "gitlab.com/terrazone/terrazone/internal/repo/battle"
)

type handler struct {
	HandlerConfig
	state *HandlerState
}

func NewHandler(hc HandlerConfig, state *HandlerState) handler {
	return handler{
		HandlerConfig: hc,
		state:         state,
	}
}

func (h handler) handleKickAss(kickAss KickAss) error {
	h.Log.Debugw("got kickAss", "in", kickAss)

	/*
		id, err := h.state.battleRepo.NewTrainingBattle()
		if err != nil {
			return err
		}

		h.state.User.BattleId = id
		h.userRepo.Update(state.User)

		h.state.
	*/

	return nil
}

func (h handler) handleStatus(status Status) error {
	return h.Send(Status{})
}

func (h handler) handleChange(change Change) error {
	h.Log.Debugw("got change", "in", change)

	/* TODO: autogenerate this crap */
	switch change.Param {
	case "note":
		h.state.User.Note = change.Value
	case "city":
		h.state.User.City = change.Value
	case "about":
		h.state.User.About = change.Value
	case "name":
		h.state.User.Name = change.Value
	case "t1":
		t1, err := strconv.Atoi(change.Value)
		h.state.User.Training1 = uint8(t1)
		if err != nil {
			h.Log.Warnw("t1 is not a number", "param", change.Param)
		}
	case "t2":
		t2, err := strconv.Atoi(change.Value)
		h.state.User.Training2 = uint8(t2)
		if err != nil {
			h.Log.Warnw("t2 is not a number", "param", change.Param)
		}
	case "clr":
		clr, err := strconv.Atoi(change.Value)
		h.state.User.Color = uint8(clr)
		if err != nil {
			h.Log.Warnw("color is not a number", "param", change.Param)
		}
	case "hint":
		hint, err := strconv.Atoi(change.Value)
		h.state.User.Hint = uint16(hint)
		if err != nil {
			h.Log.Warnw("hint is not a number", "param", change.Param)
		}
	default:
		h.Log.Warnw("unsupported changing", "param", change.Param)
		return nil
	}

	h.UserRepo.Update(context.Background(), h.state.User)

	return nil
}

func (h handler) handleGetMe(getMe GetMe) error {
	h.Log.Debugw("got getMe", "in", getMe)

	err := h.Send(StopChat{})
	if err != nil {
		return err
	}

	err = h.Send(MyParam{
		Time:         time.Now().Unix(),
		Login:        h.state.User.Login,
		Exp:          h.state.User.Exp,
		God:          h.state.User.God,
		Img:          h.state.User.Img,
		Z:            h.state.User.Z,
		BuildingType: h.state.User.BuildingType,
		Room:         h.state.User.Room,
		Training1:    h.state.User.Training1,
		Training2:    h.state.User.Training2,
		LocTime:      h.state.User.LocTime,
		BasicParam: BasicParam{
			Hp:     h.state.User.Hp,
			MaxHp:  h.state.User.MaxHp,
			Psy:    h.state.User.Psy,
			MaxPsy: h.state.User.MaxPsy,
			Level:  h.state.User.Level,
			X:      h.state.User.X,
			Y:      h.state.User.Y,
			Man:    h.state.User.Man,
			Parameters: Parameters{
				Str:   h.state.User.Str,
				Pow:   h.state.User.Pow,
				Dex:   h.state.User.Dex,
				Int:   h.state.User.Int,
				Acc:   h.state.User.Acc,
				Intel: h.state.User.Intel,
			},
		},
		Description: Description{
			Note:  h.state.User.Note,
			Name:  h.state.User.Name,
			City:  h.state.User.City,
			About: h.state.User.About,
			Hint:  h.state.User.Hint,
			Color: h.state.User.Color,
		},
		Identifiers: Identifiers{
			First:  h.state.User.First,
			Second: h.state.User.Second,
			Shift:  h.state.User.Shift,
		},
	})

	if err != nil {
		return nil
	}

	return nil
}

func (h handler) handleNop(n Nop) error {
	h.Log.Debugw("got nop", "in", n)

	if h.state.User.First != n.Id1 || h.state.User.Second != n.Id2 || h.state.User.Shift != n.I1 {
		h.state.User.First = n.Id1
		h.state.User.Second = n.Id2
		h.state.User.Shift = n.I1

		h.UserRepo.Update(context.Background(), h.state.User)
	}

	return h.Send(StopChat{})
}

func (h handler) handleGoBld(goBld GoBld) error {
	h.Log.Debugw("got gobld", "in", goBld)

	l, err := h.LocRepo.GetByXY(context.Background(), h.state.User.X, h.state.User.Y)

	if err != nil {
		return fmt.Errorf("Cannot get info about location (%d, %d): %s\n", h.state.User.X, h.state.User.Y, err)
	}

	if goBld.Number <= uint16(len(l.Buildings)) {
		h.state.User.Z = goBld.Number
		h.state.User.Room = 0
		if goBld.Number > 0 {
			h.state.User.BuildingType = uint8(l.Buildings[goBld.Number-1].Type)
		} else {
			h.state.User.BuildingType = 0
		}
	}

	h.UserRepo.Update(context.Background(), h.state.User)

	return h.Send(OkGo{})
}

func (h handler) handleGetInfo(getInfo GetInfo) error {
	u, err := h.UserRepo.GetByLogin(context.Background(), getInfo.Login)

	if err != nil {
		return h.Send(NoUser{
			Login: getInfo.Login,
		})
	}

	/* TODO: add hztxt */
	/* TODO: add description */
	return h.Send(UserParam{
		Login: getInfo.Login,
		BasicParam: BasicParam{
			Hp:     u.Hp,
			MaxHp:  u.MaxHp,
			Psy:    u.Psy,
			MaxPsy: u.MaxPsy,
			Level:  u.Level,
			X:      u.X,
			Y:      u.Y,
			Man:    u.Man,
			Parameters: Parameters{
				Str:   u.Str,
				Pow:   u.Pow,
				Dex:   u.Dex,
				Int:   u.Int,
				Acc:   u.Acc,
				Intel: u.Intel,
			},
		},
	})
}

func (h handler) Handle(data []byte) error {
	var ip InputPackage

	h.Log.Debugw("called common handler", "data", string(data))

	err := xml.Unmarshal(data, &ip)

	// TODO: wrap error
	if err != nil {
		return err
	}

	switch v := ip.Content.(type) {
	case *KickAss:
		err = h.handleKickAss(*v)
	case *GetMe:
		err = h.handleGetMe(*v)
	case *Nop:
		err = h.handleNop(*v)
	case *GoBld:
		err = h.handleGoBld(*v)
	case *Change:
		err = h.handleChange(*v)
	case *Status:
		err = h.handleStatus(*v)
	case *GetInfo:
		err = h.handleGetInfo(*v)
	default:
		// TODO: add type for error
		return fmt.Errorf("unknown content %v", v)
	}

	// TODO: wrap error
	if err != nil {
		return err
	}

	return nil
}
