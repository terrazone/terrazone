package password

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEncrypt(t *testing.T) {
	p := Encrypt("123467890123", "qwertyuopasdfh")

	assert.Equal(t, p, "58F365DE46CC40A9FCE670F67237CCC17698BD5B")
}
