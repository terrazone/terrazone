package password

import (
	"math/rand"
)

func GenSalt() string {
	s := make([]byte, 32)

	for i := 0; i < 32; i++ {
		s[i] = 'a' + byte(rand.Intn(26))
	}

	return string(s)
}
