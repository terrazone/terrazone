package password

import "fmt"

var (
	replaceTable []int = []int{
		35, 6, 4, 25, 7, 8, 36, 16, 20,
		37, 12, 31, 39, 38, 21, 5, 33, 15, 9,
		13, 29, 23, 32, 22, 2, 27, 1, 10, 30,
		24, 0, 19, 26, 14, 18, 34, 17, 28, 11, 3,
	}
)

func rol(num int32, cnt int) int32 {
	unum := uint32(num)

	return int32((unum << cnt) | (unum >> (32 - cnt)))
}

func mixup(a int32, b int32) int32 {
	c := (a & 65535) + (b & 65535)
	return ((a>>16)+(b>>16)+(c>>16))<<16 | c&65535
}

func Encrypt(d string, k string) string {
	data := []byte(fmt.Sprint(d[0:1], k[0:10], d[1:], k[10:]))
	dataLength8 := len(data) * 8

	buffer1 := make([]int32, ((dataLength8+64)>>9<<4)+16)

	for i := 0; i < dataLength8; i += 8 {
		buffer1[i>>5] = buffer1[i>>5] | (int32(data[i>>3])&0xFF)<<(24-(i&0x1F))
	}

	buffer1[dataLength8>>5] = buffer1[dataLength8>>5] | 128<<(24-(dataLength8&0x1F))
	buffer1[len(buffer1)-1] = int32(dataLength8)

	buffer2 := make([]int32, 80)

	var _loc13 int32 = 1732584193
	var _loc6 int32 = -271733879
	var _loc8 int32 = -1732584194
	var _loc7 int32 = 271733878
	var _loc14 int32 = -1009589776

	var _loc23 int32 = 0
	var _loc10 int32 = 0

	for i := 0; i < len(buffer1); i += 16 {
		_loc32 := _loc13
		_loc31 := _loc6
		_loc30 := _loc8
		_loc29 := _loc7
		_loc28 := _loc14

		for k := 0; k < 80; k++ {
			if k < 16 {
				buffer2[k] = buffer1[i+k]
			} else {
				_loc16 := buffer2[k-3] ^ buffer2[k-8] ^ buffer2[k-14] ^ buffer2[k-16]
				buffer2[k] = rol(_loc16, 1)
			}

			_loc20 := rol(_loc13, 5)
			_loc15 := mixup(_loc14, buffer2[k])

			if k < 20 {
				_loc23 = 1518500249
			} else if k < 40 {
				_loc23 = 1859775393
			} else if k < 60 {
				_loc23 = -1894007588
			} else {
				_loc23 = -899497514
			}

			_loc21 := mixup(_loc15, _loc23)

			if k < 20 {
				_loc10 = int32(int64(_loc6&_loc8) | (int64(_loc6)^(0xFFFFFFFF))&int64(_loc7))
			} else if k < 40 {
				_loc10 = (_loc6 ^ _loc8 ^ _loc7)
			} else if k < 60 {
				_loc10 = (_loc6&_loc8 | _loc6&_loc7 | _loc8&_loc7)
			} else {
				_loc10 = (_loc6 ^ _loc8 ^ _loc7)
			}

			_loc19 := mixup(_loc20, _loc10)
			_loc22 := mixup(_loc19, _loc21)

			_loc14 = _loc7
			_loc7 = _loc8
			_loc8 = rol(_loc6, 30)
			_loc6 = _loc13
			_loc13 = _loc22
		}

		_loc13 = mixup(_loc13, _loc32)
		_loc6 = mixup(_loc6, _loc31)
		_loc8 = mixup(_loc8, _loc30)
		_loc7 = mixup(_loc7, _loc29)
		_loc14 = mixup(_loc14, _loc28)
	}

	output := []int32{_loc13, _loc6, _loc8, _loc7, _loc14}

	hexchars := "0123456789ABCDEF"

	retval := make([]byte, 40)

	for i := 0; i < 20; i++ {
		retval[replaceTable[i*2]] = hexchars[(output[i>>2]>>((3-i%4)*8+4))&15]
		retval[replaceTable[i*2+1]] = hexchars[(output[i>>2]>>((3-i%4)*8))&15]
	}

	return string(retval)
}
