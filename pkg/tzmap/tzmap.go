package tzmap

import (
	"bufio"
	"bytes"
	"fmt"
	"golang.org/x/text/encoding/charmap"
	"io"
)

var (
	ErrUnsupportedFileFormat = fmt.Errorf("Unsupported file format")
	ErrUnsupportedVersion    = fmt.Errorf("Unsupported tzm version")
	ErrBrokenFile            = fmt.Errorf("Broken file")
)

type Location struct {
	Type      uint8 // Тип местности, 0 - прочее, 1 - камень, 2 - трава, 3 - пустыня
	Radiation uint8 // Уровень радиационного заражения

	// Направление дороги
	// 1 - Северо-Запад
	// 2 - Запад
	// 4 - Юго-Запад
	// 8 - Север
	// 16 - Юг
	// 32 - Северо-Восток
	// 64 - Восток
	// 128 - Юго-Восток
	Road uint8

	// Мобы
	// 1 - Крыса
	// 2 - Стич
	// 3 - Жужик
	// 4 - Студень
	// 5 - Динго
	// 6 - Скорпионы
	// 7 - Прочее
	Bot      uint8 // Мобы
	BotCount uint8 // Количество мобов на локации

	// Здания
	// 1 - Купол
	// 2 - Шахта
	// 4 - Завод
	// 8 - Лаба
	// 16 - Офис
	// 32 - Портал
	// 128 - Иное
	Building uint8 // Здания

	Labels   uint8    // Метки tzm-карты, вещь нам бесполезная
	Comments []string // Комментарии к локации, могут содержать информацию, но не парсятся практически
}

var ()

type TzMap [361][361]Location

func loadTypes(br *bufio.Reader, m *TzMap) error {
	x, y := -180, -180

	for y <= 180 {
		b, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		i := uint32(b)&0x3F + 1

		for i > 0 && y <= 180 {
			m[x+180][y+180].Type = b >> 6

			x++
			i--
			if x > 180 {
				y++
				x = -180
			}
		}
	}

	return nil
}

func loadMobs(br *bufio.Reader, m *TzMap) error {
	x, y := -180, -180

	for y <= 180 {
		var i uint32

		d, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		e, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		if e < 128 {
			i = 1
		} else {
			e = e - 128
			b, err := br.ReadByte()
			if err != nil {
				return ErrBrokenFile
			}
			i = uint32(b) + 2
		}

		for i > 0 && y <= 180 {
			m[x+180][y+180].Bot = d
			m[x+180][y+180].BotCount = e

			x++
			i--
			if x > 180 {
				y++
				x = -180
			}
		}
	}

	return nil
}

func loadRadiations(br *bufio.Reader, m *TzMap) error {
	x, y := -180, -180

	for y <= 180 {
		var i uint32

		b, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		if b < 128 {
			i = 1
		} else {
			i = uint32(b) - 128
			b = 0
		}

		for i > 0 && y <= 180 {
			m[x+180][y+180].Radiation = b

			x++
			i--
			if x > 180 {
				y++
				x = -180
			}
		}
	}

	return nil
}

func loadBuildings(br *bufio.Reader, m *TzMap) error {
	x, y := -180, -180

	for y <= 180 {
		var i uint32

		d, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		if d > 0 {
			i = 1
		} else {
			b, err := br.ReadByte()
			if err != nil {
				return ErrBrokenFile
			}
			i = uint32(b)
		}

		for i > 0 && y <= 180 {
			m[x+180][y+180].Building = d
			x++
			i--
			if x > 180 {
				y++
				x = -180
			}
		}
	}

	return nil
}

func loadRoads(br *bufio.Reader, m *TzMap) error {
	x, y := -180, -180

	for y <= 180 {
		var i uint32

		b, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		for b > 1 {
			b -= 2
			x++

			if x > 180 {
				y++
				x = -180
			}
		}

		if y <= 180 {
			b, err = br.ReadByte()
			if err != nil {
				return ErrBrokenFile
			}

			m[x+180][y+180].Road = b

			x++
			i--
			if x > 180 {
				y++
				x = -180
			}
		}
	}

	return nil
}

func loadLabels(br *bufio.Reader, m *TzMap) error {
	x, y := -180, -180

	for y <= 180 {
		d, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		e, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		i := (uint32(d)&0xF)<<8 + uint32(e)

		for ; i > 0; i-- {
			x++
			if x > 180 {
				y++
				x = -180
			}
		}

		if y <= 180 {
			m[x+180][y+180].Labels = d >> 4

			x++
			if x > 180 {
				y++
				x = -180
			}
		}
	}

	return nil
}

func loadComments(br *bufio.Reader, m *TzMap) error {
	dec := charmap.Windows1251.NewDecoder()

	x, y := -180, -180
	for y <= 180 {
		b, err := br.ReadByte()
		if err != nil {
			return ErrBrokenFile
		}

		if b < 128 {
			d, err := br.ReadByte()
			if err != nil {
				return ErrBrokenFile
			}

			i := (uint32(b) << 8) + uint32(d)

			for ; i > 0; i-- {
				x++
				if x > 180 {
					y++
					x = -180
				}
			}
		} else {
			n := (int(b) - 128) << 8

			b, err = br.ReadByte()
			if err != nil {
				return ErrBrokenFile
			}

			n += int(b)

			m[x+180][y+180].Comments = make([]string, n)

			for i := 0; i < n; i++ {
				buf, err := br.ReadBytes(0)
				if err != nil {
					return ErrBrokenFile
				}

				// decode from cp1251
				buf, err = dec.Bytes(buf)
				if err != nil {
					return ErrBrokenFile
				}

				m[x+180][y+180].Comments[i] = string(buf)
			}

			x++
			if x > 180 {
				y++
				x = -180
			}
		}
	}

	return nil
}

func LoadMap(r io.Reader) (*TzMap, error) {
	var m TzMap
	magic := make([]byte, 4)

	br := bufio.NewReader(r)

	_, err := io.ReadFull(br, magic)
	if err == io.ErrUnexpectedEOF || err == io.EOF {
		return nil, ErrUnsupportedFileFormat
	} else if err != nil {
		return nil, err
	}

	if !bytes.Equal(magic[:3], []byte{'T', 'Z', 'M'}) {
		return nil, ErrUnsupportedFileFormat
	}

	if magic[3] != 2 {
		return nil, ErrUnsupportedVersion
	}

	loaders := [](func(*bufio.Reader, *TzMap) error){
		loadTypes,
		loadMobs,
		loadRadiations,
		loadBuildings,
		loadRoads,
		loadLabels,
		loadComments,
	}

	for _, load := range loaders {
		err = load(br, &m)
		if err != nil {
			return nil, err
		}
	}

	return &m, nil
}
