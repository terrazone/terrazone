package tzmap

import (
	"bytes"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	loc00 = Location{
		Radiation: 10,
		Type:      1,
		BotCount:  5,
		Labels:    9,
		Road:      8,
		Bot:       16,
		Building:  8,
		Comments:  []string{"Комментарий\x00"},
	}
)

func TestLoadMap(t *testing.T) {
	assert := assert.New(t)

	file, err := os.Open("../../test/map.tzm")
	assert.Nil(err, "Cannot open test file")

	defer file.Close()

	m, err := LoadMap(file)

	assert.Nil(err, "File should be correctly parsed")

	assert.Equal(m[180][180], loc00)
}

func TestBrokenLoadMap(t *testing.T) {
	assert := assert.New(t)

	r := bytes.NewReader([]byte{'T'})

	_, err := LoadMap(r)
	assert.Equal(err, ErrUnsupportedFileFormat)

	r = bytes.NewReader([]byte{'P', 'N', 'G', '2'})

	_, err = LoadMap(r)
	assert.Equal(err, ErrUnsupportedFileFormat)

	r = bytes.NewReader([]byte{'T', 'Z', 'M', '3'})

	_, err = LoadMap(r)
	assert.Equal(err, ErrUnsupportedVersion)
}

func TestCropedLoadMap(t *testing.T) {
	var i int64 = 100

	assert := assert.New(t)

	for {
		file, err := os.Open("../../test/map.tzm")
		assert.Nil(err, "Cannot open test file")

		lr := io.LimitReader(file, i)

		_, err = LoadMap(lr)

		file.Close()

		if err == nil {
			break
		}

		assert.Equal(err, ErrBrokenFile)

		i++
	}
}
