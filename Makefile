VERSION?="0.0.1"
TEST?=./...
GOFMT_FILES?=$$(find . -type f -name '*.go')

-include .env

default: build

build: fmt fmtcheck
	go build -o terrazone ./cmd/server

mapper: fmtcheck
	go build -o mapper ./cmd/mapper

fmtcheck:
	@sh -c "'$(CURDIR)/scripts/gofmtcheck.sh'"

fmt:
	gofmt -d $(GOFMT_FILES)
	gofmt -w $(GOFMT_FILES)

test:
	go test -coverprofile=coverage.out ./...
	go tool cover -html=coverage.out

.PHONY: default build fmt fmtcheck test
