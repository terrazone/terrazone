module gitlab.com/terrazone/terrazone

go 1.14

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/stretchr/testify v1.5.1
	github.com/urfave/cli/v2 v2.2.0
	go.uber.org/zap v1.15.0
	golang.org/x/text v0.3.2
)
